ADMIN.api = {
    login: 'http://ic.sportsdiary.co.kr/api/loginout/login.asp',
    loginChk: 'http://ic.sportsdiary.co.kr/api/loginout/loginChk.asp',
    logout: 'http://ic.sportsdiary.co.kr/api/loginout/logout.asp',
    menu: 'http://ic.sportsdiary.co.kr/api/menu_control/menu.asp',
    notice: 'http://ic.sportsdiary.co.kr/api/main/notice.asp',
    reservation: 'http://ic.sportsdiary.co.kr/api/main/reservation.asp',
};