<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
	<meta name="apple-mobile-web-app-title" content="이천훈련원" />
	<meta name="format-detection" content="telephone=no" />
	<title>이천훈련원</title>
	
	<link rel="stylesheet" type="text/css" href="/front/css/fonts/NotoKR.css">
	<link rel="stylesheet" type="text/css" href="/front/css/admin/initialize.css">
	<link rel="stylesheet" type="text/css" href="/front/css/admin/join.css">

	<script src="/front/js/library/jquery/jquery-3.4.1.min.js"></script>
	<script src="http://img.sportsdiary.co.kr/lib/vue/vue.min.js"></script>
	<script src="http://img.sportsdiary.co.kr/lib/vue/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.min.js"></script>
</head>
<body>
	<h1 class="hidden">이천훈련원</h1>
