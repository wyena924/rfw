<!--#include virtual="/include/join_header.asp"-->

<body>
  <div id="l_join">
    <div id="l_join__wrap">
      <div id="admin_join_box">
        <div id="in_list">
          <!-- login mark -->
          <div class="l_mark">
            <img src="./front/img/Logo_Icheon.png" />
          </div>
          <!-- login -->
          <div class="l_join__box_wrap">
            <div>
              <p>"안녕하세요.</p>
              <p>선수가 중심이라는 마음으로</p>
              <p>미래를 생각하는 이천훈련원 입니다."</p>
            </div>
          </div>
        </div>

        <div id="l_join_board" class="logo s_tab_board__con">
          <div class="m_join__title">
            <h1>회원가입신청</h1>
          </div>
          <div class="m_join__success">
            <div class="m_join__success_notice">
              <p class="s_blue_txt">회원가입 신청이 완료되었습니다 !</p>
              <p class="s_gray_txt">가입 승인여부는 승인과 동시에</p>
              <p class="s_gray_txt">문자로 발송됩니다.</p>
            </div>
            <div class="m_form__btns">
              <button class="m_btn s_blue" onclick="location.href='/index.asp';">확인</button>
            </div>
          </div>
        </div>

        <div id="l_notice">
          <span class="copy_text">온라인 입퇴촌 신청 사용문의/장애처리</span>
          <span class="copy_text">전화 : 041)589-0940</span>
          <span class="copy_text">내선 : 102</span>
        </div>
      </div>
    </div>
  </div>
</body>
</html>